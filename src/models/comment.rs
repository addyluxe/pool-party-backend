#[derive(Debug, Queryable, Serialize, Deserialize)]
pub struct Comment {
    pub id: i32,
    pub text: Option<String>,
}
