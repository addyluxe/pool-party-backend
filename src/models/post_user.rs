#[derive(Deserialize)]
pub struct PostUser {
    pub username: String,
    pub email: String,
}
