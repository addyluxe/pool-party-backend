#[derive(Debug, Queryable, Serialize, Deserialize)]
pub struct Pool {
    pub id: i32,
    pub name: String,
}
