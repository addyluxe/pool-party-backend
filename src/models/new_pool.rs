use schema::pools;

#[derive(Insertable)]
#[table_name="pools"]
pub struct NewPool<'a> {
    pub name: &'a str,
}
