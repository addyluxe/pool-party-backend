use schema::comments;

#[derive(Insertable)]
#[table_name="comments"]
pub struct NewComment<'a> {
    pub text: &'a str,
}
