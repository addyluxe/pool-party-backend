table! {
    comments (id) {
        id -> Int4,
        text -> Nullable<Text>,
    }
}

table! {
    pools (id) {
        id -> Int4,
        name -> Varchar,
    }
}

table! {
    users (id) {
        id -> Int4,
        username -> Varchar,
        email -> Varchar,
    }
}

allow_tables_to_appear_in_same_query!(
    comments,
    pools,
    users,
);
