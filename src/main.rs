extern crate env_logger;
extern crate actix_web;
#[macro_use] extern crate serde_derive;

#[macro_use]
extern crate diesel;
extern crate dotenv;

use actix_web::{server, middleware, App, Json, Result, http, Path};
use diesel::prelude::*;
use diesel::pg::PgConnection;
use dotenv::dotenv;
use std::env;
mod models;
mod schema;

fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

fn create_user(username: &str, email: &str) -> models::User {
    use schema::users;

    let conn = establish_connection();

    let new_user = models::NewUser {
        username: username,
        email: email,
    };

    diesel::insert_into(users::table)
        .values(&new_user)
        .get_result(&conn)
        .expect("Error creating user")
}

fn create_pool(name: &str) -> models::Pool {
    use schema::pools;
    
    let conn = establish_connection();

    let new_pool = models::NewPool {
        name: name,
    };

    diesel::insert_into(pools::table)
        .values(&new_pool)
        .get_result(&conn)
        .expect("Error creating pool")
}

fn create_comment(text: &str) -> models::Comment {
    use schema::comments;

    let conn = establish_connection();

    let new_comment = models::NewComment {
        text: text,
    };

    diesel::insert_into(comments::table)
        .values(&new_comment)
        .get_result(&conn)
        .expect("Error creating comment")
}

fn show_user(req: Path<(models::Id)>) -> Result<Json<models::User>> {
    use schema::users;
    let conn = establish_connection();
    let user = users::table.find(req.id).first::<models::User>(&conn).expect("Error finding user");
    Ok(Json(models::User{id: user.id, username: user.username, email: user.email}))
}

fn user(user: Json<models::PostUser>) -> Result<String> {
    let created = create_user(&user.username, &user.email);
    Ok(format!("Created user: {:?}", created))
}

fn show_pool(req: Path<(models::Id)>) -> Result<Json<models::Pool>> {
    use schema::pools;
    let conn = establish_connection();
    let pool = pools::table.find(req.id).first::<models::Pool>(&conn).expect("Error finding pool");
    Ok(Json(models::Pool{id: pool.id, name: pool.name}))
}

fn pool(pool: Json<models::PostPool>) -> Result<String> {
    let created = create_pool(&pool.name);
    Ok(format!("Created pool: {:?}", created))
}

fn show_comment(req: Path<(models::Id)>) -> Result<Json<models::Comment>> {
    use schema::comments;
    let conn = establish_connection();
    let comment = comments::table.find(req.id).first::<models::Comment>(&conn).expect("Error finding comment");
    Ok(Json(models::Comment{id: comment.id, text: comment.text}))
}

fn comment(comment: Json<models::PostComment>) -> Result<String> {
    let created = create_comment(&comment.text);
    Ok(format!("Created comment: {:?}", created))
}

fn main() {
    ::std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();
    
    server::new(
        || App::new()
            .middleware(middleware::Logger::default())
            .resource("/user/{id}", |r| r.method(http::Method::GET).with(show_user))
            .resource("/user", |r| r.method(http::Method::POST).with(user))
            .resource("/pool/{id}", |r| r.method(http::Method::GET).with(show_pool))
            .resource("/pool", |r| r.method(http::Method::POST).with(pool))
            .resource("/comment/{id}", |r| r.method(http::Method::GET).with(show_comment))
            .resource("/comment", |r| r.method(http::Method::POST).with(comment)))
        .bind("0.0.0.0:1337").expect("Can not bind to 0.0.0.0:1337")
        .run();
}
